package com.adaro.dispatcher;

import java.util.List;

public class SessionData {

    private Credential sessionCredential;
    private List<Unit> activeUnits;
    private List<Unit> breakdownUnits;
    private boolean dataStatus;

    public SessionData() {
    }

    public SessionData(Credential sessionCredential, List<Unit> activeUnits, List<Unit> breakdownUnits, boolean dataStatus) {
        this.sessionCredential = sessionCredential;
        this.activeUnits = activeUnits;
        this.breakdownUnits = breakdownUnits;
        this.dataStatus = dataStatus;
    }

    public Credential getSessionCredential() {
        return sessionCredential;
    }

    public void setSessionCredential(Credential sessionCredential) {
        this.sessionCredential = sessionCredential;
    }

    public List<Unit> getActiveUnits() {
        return activeUnits;
    }

    public void setActiveUnits(List<Unit> activeUnits) {
        this.activeUnits = activeUnits;
    }

    public List<Unit> getBreakdownUnits() {
        return breakdownUnits;
    }

    public void setBreakdownUnits(List<Unit> breakdownUnits) {
        this.breakdownUnits = breakdownUnits;
    }

    public boolean isDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(boolean dataStatus) {
        this.dataStatus = dataStatus;
    }

    @Override
    public String toString() {
        return "SessionData{" +
                "sessionCredential=" + sessionCredential +
                ", activeUnits=" + activeUnits +
                ", breakdownUnits=" + breakdownUnits +
                ", dataStatus=" + dataStatus +
                '}';
    }

}
