package com.adaro.dispatcher;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity{
    private static final String TAG = "RegisterActivityDev";
    private EditText mEmail, mUsername, mPassword, mPhone;
    private Button mSignUp;
    private String email, username, password, phone;
    private Switch switchFlagSeller;
    private Boolean flagSeller = Boolean.FALSE;
    /* Firebase */
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseMethod firebaseMethod;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);
        firebaseMethod = new FirebaseMethod(RegisterActivity.this);
        Log.d(TAG, "onCreate: created.");
        setupFirebaseAuth();
        initWidget();
        init();
    }

    private void initWidget() {
        Log.d(TAG, "initWidget: initializing widget.");

        mEmail = findViewById(R.id.userMailDev);
        mUsername = findViewById(R.id.userNameDev);
        mPassword = findViewById(R.id.passwordUser);
        mPhone = findViewById(R.id.phoneNumber);
        mSignUp = findViewById(R.id.signInBtn);
    }

    private void init() {
        Log.d(TAG, "init: initializing button.");
        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: attempting to submit.");

                email = mEmail.getText().toString();
                username = mUsername.getText().toString();
                password = mPassword.getText().toString();
                phone = mPhone.getText().toString();

                if (checkInputs(email, username, password)) {
                    firebaseMethod.registerNewEmail(email, password);
                }
            }
        });
    }

    private boolean checkInputs(String email, String username, String password) {
        Log.d(TAG, "checkInputs: validating inputs");
        if (email.equals("") || username.equals("") || password.equals("")) {
            showSnackbar("Isi field yang mandatory yaa");
            return false;
        } else {
            if (!(email.contains("@") && email.contains("."))) {
                showSnackbar("Email kamu tidak valid.");
                return false;
            }
            if (password.length() < 6) {
                showSnackbar("Isi password lebih dari 5 karakter yaa");
                return false;
            }
        }
        return true;
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth");
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    Log.d(TAG, "onAuthStateChangedRegister: signed_in with (" + firebaseUser.getUid() + ")");
                    firebaseMethod.addNewUser(firebaseUser.getUid(), "pengawas");
                    Toast.makeText(RegisterActivity.this,
                            "Pendafatan sukses, mohon konfirmasi email kamu.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Log.d(TAG, "onAuthStateChangedRegister: signed_out");
                }
            }
        };
    }

    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    public void onStop() {
        super.onStop();
        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    public Boolean getFlagSeller() {
        return flagSeller;
    }

    public void setFlagSeller(Boolean flagSeller) {
        this.flagSeller = flagSeller;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }
}
