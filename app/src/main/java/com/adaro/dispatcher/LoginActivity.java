package com.adaro.dispatcher;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Hi!
 * Created by captain_n3mo on 25/03/2018.
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivityDev";

    /* Widget */
    private EditText mEmail, mPassword;
    private TextView linkSignUp;
    private Button mLogin;

    /* Firebase */
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        Log.d(TAG, "onCreate: started.");

        /* setting up firebase */
        setupFirebaseAuth();

        /* setting up widget */
        initWidget();

        /* setting up button login*/
        initButton();
    }

    private void initWidget() {
        mEmail = findViewById(R.id.emailUser);
        mPassword = findViewById(R.id.passwordUserLogin);
        linkSignUp = findViewById(R.id.registerAccount);
        linkSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: navigating to registrer screen");
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
    }

    private boolean isEmailOrPasswordNull(String email, String password) {
        if (email.equals("") || password.equals("")) {
            return true;
        }
        return false;
    }

    //initialize button login
    private void initButton() {
        mLogin = findViewById(R.id.signInBtn);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: atempting to login");
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                boolean checkNull = isEmailOrPasswordNull(email, password);
                if (checkNull) {
                    Log.d(TAG, "onClick: email or password are null");
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                            "Mohon isi email dan password", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(
                            LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(TAG, "signInWithEmail:onComplete: " + task.isSuccessful());

                                    if (!task.isSuccessful()) {
                                        Log.d(TAG, "signInWithEmail:onFailed:", task.getException());
                                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                                                "Kami belum mempunyai akun kamu, yuk buat akun.", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    } else {
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                }
                            });
                }
            }
        });

        /* if the user is logged in then navigate to home activity and call 'finish()'*/
        if (firebaseAuth.getCurrentUser() != null) {
            Log.d(TAG, "initButton: navigating to home screen.");

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth");
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    Log.d(TAG, "onAuthStateChangedLogin: signed_in with (" + firebaseUser.getUid() + ")");
                } else {
                    Log.d(TAG, "onAuthStateChangedLogin: signed_out");
                }
            }
        };
    }

    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    public void onStop() {
        super.onStop();
        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

}
