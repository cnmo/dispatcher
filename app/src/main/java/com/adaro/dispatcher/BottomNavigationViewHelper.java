package com.adaro.dispatcher;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class BottomNavigationViewHelper {
    private static final String TAG = "BottomNavigationViewHel";

    public static void setupBottomNavigationView(BottomNavigationViewEx bottomNavigationViewEx) {
        Log.d(TAG, "setupBottomNavigationView: Setting up navigation view");
        bottomNavigationViewEx.enableAnimation(true);
        bottomNavigationViewEx.enableItemShiftingMode(true);
        bottomNavigationViewEx.enableShiftingMode(true);
        bottomNavigationViewEx.setTextVisibility(true);
    }

    public static void enableNavigation(final Context context, BottomNavigationViewEx viewEx) {
        Log.d(TAG, "enableNavigation: Enabling navigation");
        viewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent intentNearby = new Intent(context, MainActivity.class);
                        intentNearby.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(intentNearby);
                        return true;
                    case R.id.navigation_dashboard:
                        Intent profileIntent = new Intent(context, ReportActivity.class);
                        profileIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(profileIntent);
                        return true;
                    case R.id.navigation_account:
                        Intent notificationIntent = new Intent(context, NotificationActivity.class);
                        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(notificationIntent);
                        return true;
                    case R.id.navigation_setting:
                        Intent notificationSetting = new Intent(context, SettingActivity.class);
                        notificationSetting.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(notificationSetting);
                        return true;
                }
                return false;
            }
        });
    }
}
