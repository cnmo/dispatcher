package com.adaro.dispatcher;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Hi!
 * Created by captain_n3mo on 25/03/2018.
 */
public class FirebaseMethod {

    private static final String TAG = "FirebaseMethod";

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private Context mContext;

    public FirebaseMethod(Context context) {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        mContext = context;
    }

    /**
     * Register a new email and password to Firebase Authentication.
     * @param email
     * @param password
     */
    public void registerNewEmail(final String email, final String password) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete: " + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Toast.makeText(mContext, "Authentication Failed", Toast.LENGTH_SHORT).show();
                        } else if (task.isSuccessful()) {
                            sendEmailVerification();
                            Log.d(TAG, "onComplete: Authenticate Changed");
                        }
                    }
                });
    }

    /**
     * sending email verification
     */
    private void sendEmailVerification() {
        FirebaseUser user = firebaseAuth.getCurrentUser();

        if (user != null) {
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {

                    } else {
                        Toast.makeText(mContext, "Tidak bisa mengirim email konfimasi", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    public void addNewUser(String userID, String flagUser) {
        User user = new User(userID, flagUser);
        databaseReference.child(mContext.getString(R.string.dbname_user))
                .child(userID)
                .setValue(user);
    }

}
