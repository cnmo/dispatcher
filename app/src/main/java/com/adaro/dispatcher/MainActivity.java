package com.adaro.dispatcher;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "NearbyActivity";

    /*authentication*/
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Bind(R.id.spinner_vendor)
    protected Spinner vendorSelect;

    @Bind(R.id.spinner_section)
    protected Spinner sectionSelect;

    @Bind(R.id.spinner_type_unit_active)
    protected Spinner activeUnitsSelect;

    @Bind(R.id.spinner_type_unit_breakdown)
    protected Spinner breakdownUnitsSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupFirebaseAuth();
        setupInfoCard();
        setupBottomNavigationView();
    }

    /*Setting up info Card*/
    private void setupInfoCard(){

        /*vendor spinner*/
        List<String> items = new ArrayList<>();
        items.add("RMI");
        items.add("Verdanco");
        ArrayAdapter<String> adapterVendor = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, items);
        vendorSelect.setAdapter(adapterVendor);

        /*section spinner*/
        List<String> itemsSeciton = new ArrayList<>();
        itemsSeciton.add("Mine Infrasturture");
        itemsSeciton.add("WWM Low Wall");
        itemsSeciton.add("WWM High Wall");
        ArrayAdapter<String> adapterSection = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, itemsSeciton);
        sectionSelect.setAdapter(adapterSection);

        /*units spinner*/
        List<String> itemsUnits = new ArrayList<>();
        itemsUnits.add("Grader");
        itemsUnits.add("PC200 LA");
        itemsUnits.add("Compactor");
        ArrayAdapter<String> adapterUnits = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, itemsUnits);
        activeUnitsSelect.setAdapter(adapterUnits);
        breakdownUnitsSelect.setAdapter(adapterUnits);

    }

    /*BottomNavigationView setup*/
    private void setupBottomNavigationView() {
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(this, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
    }

    /*start activity life cycle*/
    @Override
    protected void onStart() {
        super.onStart();
        /*check authentication*/
        firebaseAuth.addAuthStateListener(authStateListener);
        checkCurrentUser(firebaseAuth.getCurrentUser());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*removing auth listener*/
        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    /*start firebase authentication*/

    /**
     * checks to see if the @param 'firebaseUser' is logged in
     *
     * @param firebaseUser
     */
    private void checkCurrentUser(FirebaseUser firebaseUser) {
        Log.d(TAG, "checkCurrentUser: checking user data");
        if (firebaseUser == null) {
            Intent intent = new Intent(this, StartActivity.class);
            startActivity(intent);
        } else {

        }
    }

    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth");

        /*firebase auth instance*/
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                Log.d(TAG, "onAuthStateChanged: checking in firebase user in nearbyActivity");
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    //User is signed
                    Log.d(TAG, "onAuthStateChanged: signed_in" + firebaseUser.getUid());
                } else {
                    //User is signed out
                    Log.d(TAG, "onAuthStateChanged: signed_out");
                }
            }
        };
    }

}
