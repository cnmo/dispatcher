package com.adaro.dispatcher;

public class Credential {

    private String vendorName;
    private String adaroSection;
    private String location;

    public Credential() {
    }

    public Credential(String vendorName, String adaroSection, String location) {
        this.vendorName = vendorName;
        this.adaroSection = adaroSection;
        this.location = location;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getAdaroSection() {
        return adaroSection;
    }

    public void setAdaroSection(String adaroSection) {
        this.adaroSection = adaroSection;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
