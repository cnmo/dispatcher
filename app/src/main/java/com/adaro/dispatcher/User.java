package com.adaro.dispatcher;

public class User {
    private String userID;
    private String flagUser;

    public User(String userID, String flagUser) {
        this.userID = userID;
        this.flagUser = flagUser;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFlagUser() {
        return flagUser;
    }

    public void setFlagUser(String flagUser) {
        this.flagUser = flagUser;
    }
}