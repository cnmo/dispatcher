package com.adaro.dispatcher;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class SettingActivity extends AppCompatActivity {

    private static final String TAG = "SettingActivity";

    /*authentication*/
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        setupFirebaseAuth();

        setupBottomNavigationView();
    }


    /*BottomNavigationView setup*/
    private void setupBottomNavigationView() {
        Log.d("ReportActivity", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(this, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);
    }


    /*start activity life cycle*/
    @Override
    protected void onStart() {
        super.onStart();
        /*check authentication*/
        firebaseAuth.addAuthStateListener(authStateListener);
        checkCurrentUser(firebaseAuth.getCurrentUser());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*removing auth listener*/
        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    /*start firebase authentication*/
    /**
     * checks to see if the @param 'firebaseUser' is logged in
     * @param firebaseUser
     */
    private void checkCurrentUser(FirebaseUser firebaseUser) {
        Log.d(TAG, "checkCurrentUser: checking user data");
        if (firebaseUser == null) {
            Intent intent = new Intent(this, StartActivity.class);
            startActivity(intent);
        } else {

        }
    }

    /**
     * Setup the firebase auth object
     */
    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: setting up firebase auth");

        /*firebase auth instance*/
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                Log.d(TAG, "onAuthStateChanged: checking in firebase user in nearbyActivity");
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    //User is signed
                    Log.d(TAG, "onAuthStateChanged: signed_in" + firebaseUser.getUid());
                } else {
                    //User is signed out
                    Log.d(TAG, "onAuthStateChanged: signed_out");
                }
            }
        };
    }

    public void signOut(View view) {
        firebaseAuth.signOut();
        checkCurrentUser(firebaseAuth.getCurrentUser());
    }
}
