package com.adaro.dispatcher;

public class Unit {

    private String unitName;
    private String unitNo;
    private String status;

    public Unit() {
    }

    public Unit(String unitName, String unitNo, String status) {
        this.unitName = unitName;
        this.unitNo = unitNo;
        this.status = status;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
